# Jupyter notebook to generate papers, using nbconvert and a latex postprocessor

## Introduction

This is a simple template to demonstrate how fully formatted reports and papers can directly be generated from Jupyter notebooks. Although not very sophisticated, it shows how most common requirements (e.g. title block, abstract, citations, section numbering & referencing, equations, equation numbering & referencing, figure numbering & referencing, etc) can be easily met. It is based on the IEEE tutorial to do so ( and extends it by demonstrating a better way to organise the project.

## Quick start

To use this for your own papers, fork or clone this repository and edit the Jupyter notebook `paper.ipynb` and references `references.bib`. To generate the pdf version, simply open a terminal in the scripts folder and type the bash command `sh run.sh`. If everything goes well, you should have a `paper.pdf` generated in the folder, and a static folder for the generated figures.

## Dependencies

You will certainly need the following installed:

- Python 3
- Jupyter notebook
- LaTeX (installed via conda)

If you want to produce 'bokeh' interactive plots, you may need to install the libraries


- `selenium`
- `pillow`
- `phantomjs` (via npm, it is a javascript node library)

These allow high quality `bokeh` plots to be included in your paper. However, feel free to use `matplotlib` instead, as demonstrated.

## Credits

The tutorial is adapted from [Mandar Chitre](https://github.com/org-arl/jupyter-ieee-paper)
The `ieee.tplx` template is adapted from work by [Julius Schulz and Alexander Schlaich](https://github.com/schlaicha/jupyter-publication-scripts).
